package com.perpule.assignment.controller;

import com.perpule.assignment.implementation.CustomerConfigDAO;
import com.perpule.assignment.param.CustomerDetailsRequest;
import com.perpule.assignment.param.CustomerDetailsResponse;
import com.perpule.assignment.param.StoreCustomerInfoRequest;
import com.perpule.assignment.param.StoreCustomerInfoResponse;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/customerService")
public class CustomerService {

    @RequestMapping(value = "/storeCustomerInfo", method = RequestMethod.POST)
    public StoreCustomerInfoResponse storeCustomerInfo(@RequestHeader("Authorization") String authorizationText,
                                                       @RequestBody StoreCustomerInfoRequest storeCustomerInfoRequest) throws Exception{
           System.out.println("Received request param: " + storeCustomerInfoRequest);
           return CustomerConfigDAO.getInstance().storeCustomerInformation(storeCustomerInfoRequest);

    }

  @RequestMapping(value = "/getCustomerInfo/{customerKey}", method = RequestMethod.GET)
    public CustomerDetailsResponse getWorkStations(@RequestHeader("Authorization") String authorizationText,
            @PathVariable("customerKey") String customerKey) throws Exception{
            return CustomerConfigDAO.getInstance().getCustomerDetails(customerKey);

    }

}
