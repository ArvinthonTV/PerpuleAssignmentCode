package com.perpule.assignment.implementation;

import com.perpule.assignment.config.DataSourceConfig;
import com.perpule.assignment.param.*;
import org.apache.coyote.http2.ConnectionException;

import java.sql.*;

public class CustomerConfigDAOImpl implements CustomerConfigDAO {

    DataSourceConfig dbConfig;
    Connection connection = null;

    public CustomerConfigDAOImpl(){
        try{
            if(dbConfig==null){
                connection = new DataSourceConfig().getConnection();
            }
        }
        catch(Exception e){
            e.printStackTrace();
        }

    }


    public CustomerDetailsResponse getCustomerDetails(String customerKey) throws Exception{
        CustomerDetailsResponse customerDetailsResponse = new CustomerDetailsResponse();
        ResultSet resultSet = null;
        try (Statement statement = connection.createStatement()) {
            String query="SELECT * FROM ARVINTHONDATABASE.CUSTOMER WHERE CUSTOMERID="
                    + "'" + customerKey + "'";
            resultSet = statement.executeQuery(query);
            while (resultSet.next()) {
                CustomerDetails customerDetails = new CustomerDetails();
                customerDetails.setCustomerID(resultSet.getString(ServiceConstants.ONE));
                customerDetails.setFirstName(resultSet.getString(ServiceConstants.TWO));
                customerDetails.setLastName(resultSet.getString(ServiceConstants.THREE));
                customerDetails.setDateOfBirth(resultSet.getString(ServiceConstants.FOUR));
                customerDetails.setAge(resultSet.getString(ServiceConstants.FIVE));
                customerDetails.setBloodGroup(resultSet.getString(ServiceConstants.SIX));
                customerDetails.setPhoneNo(resultSet.getString(ServiceConstants.SEVEN));
                customerDetails.setOccupation(resultSet.getString(ServiceConstants.EIGHT));
                customerDetailsResponse.setCustomerDetails(customerDetails);
            }
        }
        catch(Exception e){
            throw new SQLException(e);
        }
        return customerDetailsResponse;
    }

    public StoreCustomerInfoResponse storeCustomerInformation(StoreCustomerInfoRequest storeCustomerInfoRequest) throws Exception{
        StoreCustomerInfoResponse storeCustomerInfoResponse = new StoreCustomerInfoResponse();
       String  query = "INSERT INTO ARVINTHONDATABASE.CUSTOMER" +
               "(CUSTOMERID, FIRSTNAME, LASTNAME, DOB, AGE, BLOODGROUP, PHONENO, " +
               "OCCUPATION,RECCRTNUSERID, RECCRTNTIME) " +
                " VALUES (?,?,?,?,?,?,?,?,?,?)";
        try(PreparedStatement statement = connection.prepareStatement(query)){
            statement.setString(ServiceConstants.ONE, storeCustomerInfoRequest.getCustomerDetails().getCustomerID());
            statement.setString(ServiceConstants.TWO, storeCustomerInfoRequest.getCustomerDetails().getFirstName());
            statement.setString(ServiceConstants.THREE, storeCustomerInfoRequest.getCustomerDetails().getLastName());
            statement.setString(ServiceConstants.FOUR, storeCustomerInfoRequest.getCustomerDetails().getDateOfBirth());
            statement.setString(ServiceConstants.FIVE, storeCustomerInfoRequest.getCustomerDetails().getAge());
            statement.setString(ServiceConstants.SIX, storeCustomerInfoRequest.getCustomerDetails().getBloodGroup());
            statement.setString(ServiceConstants.SEVEN, storeCustomerInfoRequest.getCustomerDetails().getPhoneNo());
            statement.setString(ServiceConstants.EIGHT, storeCustomerInfoRequest.getCustomerDetails().getOccupation());
            statement.setString(ServiceConstants.NINE, ServiceConstants.USERID);
            statement.setString(ServiceConstants.TEN, " ");
            statement.execute();
            storeCustomerInfoResponse.setStorageStatus(ServiceConstants.SAVE_MSG);
        }
        catch (SQLException e){
            throw new SQLException(e);
        }

        return storeCustomerInfoResponse;


    }
}
