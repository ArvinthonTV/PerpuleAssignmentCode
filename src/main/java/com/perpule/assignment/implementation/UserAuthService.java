package com.perpule.assignment.implementation;

import com.perpule.assignment.param.ValidateLogonResponse;

public interface UserAuthService {

    public static UserAuthService dataSourceinstance = new UserAuthServiceImpl();

    public static UserAuthService getInstance() {
        UserAuthService userAuthService = dataSourceinstance;
        return userAuthService;
    }

    public ValidateLogonResponse validateUser(String username, String password);
}
