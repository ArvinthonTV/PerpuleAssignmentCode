package com.perpule.assignment.interceptor;

import com.perpule.assignment.implementation.SecurityService;
import com.perpule.assignment.param.TokenResponseModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;

import static com.perpule.assignment.param.ServiceConstants.AUTHTOKEN;
import static com.perpule.assignment.param.ServiceConstants.HEADER_STRING;
import static com.perpule.assignment.param.ServiceConstants.TRUSTED;

/**
 * Created by 31250 on 5/25/2018.
 */
@Component
public class SecurityInterceptor implements HandlerInterceptor {

    @Autowired
    SecurityService securityService;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

        if(request.getHeader("requestMode").equals(AUTHTOKEN)){
            return true;
        }
        else if(request.getHeader("requestMode").equals(TRUSTED)){
            System.out.println("=====================================================");
            System.out.println("Inside SecurityInterceptor ELSE IF- pre handle mtd");
            return verifyRequest(request, response);

        }

        return false;

    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView
            modelAndView) throws Exception {
        // Do Nothing as currently no requirement to handle any post handle calls
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
            throws Exception {
        // Do Nothing as currently no requirement to handle any After Completion calls
    }

    /**
     * Verifies if the request contains a valid JWT Token string.
     */
    private boolean verifyRequest(HttpServletRequest request, HttpServletResponse response) throws IOException {
        System.out.println("Inside verifyRequest mtd");
        //Retrieves the authorization parameter from the request.
        String authorizationParameter = request.getHeader(HEADER_STRING); //sonar request this at line 5.

        //Verifies if the authorization parameter is present.
        if (authorizationParameter == null) {

            //Writes an error response.
           // writeAuthorizationError(response);

            //Breaks the filter chain.
            return false;
        }

        //Verifies the received token information.
        TokenResponseModel tokenItem = securityService.verifyToken(authorizationParameter);
        System.out.println("TokenResponseModel: "+tokenItem);

        //Checks if the verified token process was okay.
        if (!tokenItem.isVerified()) {

            //Writes the verification error and the token's moreInfo errors to the response.
          //  writeVerificationError(response, tokenItem);

            //Breaks the filter chain.
            return false;
        }

        //Continues the filter chain.
        return true;
    }


}
