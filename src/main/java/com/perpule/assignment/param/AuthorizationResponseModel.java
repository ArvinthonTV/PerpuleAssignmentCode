package com.perpule.assignment.param;

/**
 * Created by 31250 on 5/25/2018.
 */
public class AuthorizationResponseModel {

/*
  @ApiModelProperty(notes = "Contains the JWT token string which will be returned to the user.", dataType = "String",
          example =
                  "yJhbGciOiJIUzUxMiJ9.eyJ3b3Jrc3RhdGlvbklkIjoicGVyc29uYWxfbGFwdG9wXzQ4NjIiLCJ0b2tlblR5cGUiOiJBbm" +
                          "9ueW1vdXMiLCJ1c2VySWQiOm51bGx9.9_xS8dN_rkFqNMKyPUsiK7qI6KJq9FnFD1iJomTybu-yi84usgz52" +
                          "qU8mJ8bEmjSmsYwkGd6Y9Fc1CgjiPc59Q")
*/
  private String authorizationText;
  private boolean verified;
  // private List<AdditionalError> moreInfoList;

  /**
   * AuthorizationResponseModel
   */
  public AuthorizationResponseModel() {

    this.authorizationText = "";
    this.verified = false;
  }

  public String getAuthorizationText() {
    return authorizationText;
  }

  public void setAuthorizationText(String authorizationText) {
    this.authorizationText = authorizationText;
  }

  public boolean isVerified() {
    return verified;
  }

  public void setVerified(boolean verified) {
    this.verified = verified;
  }


  @Override
  public String toString() {
    return "{" +
            "'authorizationText': '" + authorizationText + "'" +
            "}";
  }
}
