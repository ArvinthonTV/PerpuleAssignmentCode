package com.perpule.assignment.param;

import java.util.List;

/**
 * Base error model for version2
 */


public class CustomError {

  private int code;
  private String message;
  private String developerMessage;
 // private List<MoreInfo> moreInfo;

  /**
   * This is a default constructor
   */
  public CustomError(){
    //default Constructor
  }

  /**
   * This is to populate the error response with code,message,
   * developer message along with optional moreinfo
   *
   * @param code
   * @param message
   * @param developerMessage
   * @param moreInfo
   */

  /*public Error(String code, String message, String developerMessage, List<MoreInfo> moreInfo) {
    this.code = code;
    this.message = message;
    this.developerMessage = developerMessage;
     this.moreInfo = moreInfo;
  }*/

  /**
   * This is to populate the error response with code,message,
   * developer message
   *
   * @param code
   * @param message
   * @param developerMessage
   */
  public CustomError(int code, String message, String developerMessage) {
    this.code = code;
    this.message = message;
    this.developerMessage = developerMessage;
  }

  public int getCode() {
    return code;
  }

  public void setCode(int code) {
    this.code = code;
  }

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  public String getDeveloperMessage() {
    return developerMessage;
  }

  public void setDeveloperMessage(String developerMessage) {
    this.developerMessage = developerMessage;
  }

/*  public List<MoreInfo> getMoreInfo() {
    return moreInfo;
  }

  public void setMoreInfo(List<MoreInfo> moreInfo) {
    this.moreInfo = moreInfo;
  }*/

  @Override
  public String toString() {
    return "Error{" +
            "code='" + code + '\'' +
            ", message='" + message + '\'' +
            ", developerMessage='" + developerMessage + '\'' +
            // ", moreInfo=" + moreInfo +
            '}';
  }
}
