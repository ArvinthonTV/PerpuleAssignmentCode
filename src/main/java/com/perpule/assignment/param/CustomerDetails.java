package com.perpule.assignment.param;

public class CustomerDetails {

    private String customerID;
    private String firstName;
    private String lastName;
    private String dateOfBirth;
    private String age;
    private String bloodGroup;
    private String phoneNo;
    private String occupation;

    public String getCustomerID() {
        return customerID;
    }

    public void setCustomerID(String customerID) {
        this.customerID = customerID;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getBloodGroup() {
        return bloodGroup;
    }

    public void setBloodGroup(String bloodGroup) {
        this.bloodGroup = bloodGroup;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public String getOccupation() {
        return occupation;
    }

    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }

    @Override
    public String toString() {
        return "[firstName: "+ firstName + "," +
                "lastName: "+ lastName + "," +
                "dateOfBirth: "+ dateOfBirth + "," +
                "age: "+ age + "," +
                "bloodGroup: "+ bloodGroup + "," +
                "phoneNo: "+ phoneNo + "," +
                "occupation: "+ occupation + "]";
    }
}
