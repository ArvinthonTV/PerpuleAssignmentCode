package com.perpule.assignment.param;

public class CustomerDetailsRequest {

    private int customerID;

    public int getCustomerID() {
        return customerID;
    }

    public void setCustomerID(int customerID) {
        this.customerID = customerID;
    }


}
