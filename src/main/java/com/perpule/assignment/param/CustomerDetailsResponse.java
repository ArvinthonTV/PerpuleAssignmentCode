package com.perpule.assignment.param;

public class CustomerDetailsResponse {


    public CustomerDetails getCustomerDetails() {
        return customerDetails;
    }

    public void setCustomerDetails(CustomerDetails customerDetails) {
        this.customerDetails = customerDetails;
    }

    private CustomerDetails customerDetails;
}
