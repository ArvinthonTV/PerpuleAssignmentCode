package com.perpule.assignment.param;

/**
 * Created by 31250 on 5/25/2018.
 */
public class ServiceConstants {

    public static final String AUTHTOKEN = "authToken";
    public static final String TRUSTED = "trusted";
    public static final String SECRET = "secret_key_to_generate_json_web_tokens";
    public static final String ISSUER= "individual";
    public static final long EXPIRATION_TIME = 43_200_000; //12 hours
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String HEADER_STRING = "Authorization";
    public static final int UNSPECIFIED_ERROR_CODE = 100;
    public static final int AUTHORISATION_ERROR_CODE = 101;
    public static final int EXPIRATION_TIME_CODE = 102;

    public static final int LOGIN_ERROR_CODE=105;
    public static final String LOGIN_ERROR_MESSAGE = "Login Unsuccessful";
    public static final String LOGIN_ERROR_DEV_MESSAGE = "Entered username or password is incorrect. Please enter a valid username or password";
    public static final String DB_URL="jdbc:mysql://localhost:3306/arvinthondatabase";
    public static final String U_NAME="root";
    public static final String P_WORD="9600485230";
    public static final String USERID="12345";
    public static final String SAVE_MSG = "Successfully saved customer details";
    public static final int ONE = 1;
    public static final int TWO = 2;
    public static final int THREE = 3;
    public static final int FOUR = 4;
    public static final int FIVE = 5;
    public static final int SIX = 6;
    public static final int SEVEN = 7;
    public static final int EIGHT = 8;
    public static final int NINE = 9;
    public static final int TEN = 10;



}
