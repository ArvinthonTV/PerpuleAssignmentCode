package com.perpule.assignment.param;

import java.util.List;

public class StoreCustomerInfoResponse {

    public String getStorageStatus() {
        return storageStatus;
    }

    public void setStorageStatus(String storageStatus) {
        this.storageStatus = storageStatus;
    }

    private String storageStatus;

    private List<CustomError> errorList;

    public List<CustomError> getErrorList() {
        return errorList;
    }

    public void setErrorList(List<CustomError> errorList) {
        this.errorList = errorList;
    }



    @Override
    public String toString() {
        return "[storageStatus "+ storageStatus + "]";
    }



}
