package com.perpule.assignment.param;

import java.util.List;

public class TokenResponseModel {

  private String tokenString;
  // private List<AdditionalError> moreInfoList;
  private boolean verified;
  private List<CustomError> errorList;

  public String getTokenString() {
    return tokenString;
  }

  public void setTokenString(String tokenString) {
    this.tokenString = tokenString;
  }

  public boolean isVerified() {
    return verified;
  }

  public void setVerified(boolean verified) {
    this.verified = verified;
  }

  public List<CustomError> getErrorList() {
    return errorList;
  }

  public void setErrorList(List<CustomError> errorList) {
    this.errorList = errorList;
  }



  /**
   * TokenResponseModel
   *
   * @param tokenString
   */
  public TokenResponseModel(String tokenString) {

    this.tokenString = tokenString;
    this.verified = false;
    // this.moreInfoList = new ArrayList<>();
  }





/*
  public List<AdditionalError> getMoreInfoList() {
    return moreInfoList;
  }

  */
/**
   * addMoreInfoItem
   *
   * @param
   *//*

  public void addMoreInfoItem(AdditionalError moreInfoItem) {
    this.moreInfoList.add(moreInfoItem);
  }
*/

  @Override
  public String toString() {

    return "{ TokenResponseModel"+ " \"verified\": " + isVerified() + " }";
  }
}
