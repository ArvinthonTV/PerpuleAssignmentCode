package com.perpule.assignment.param;

/**
 * Created by 31250 on 5/25/2018.
 */
public class ValidateLogonResponse {
    private boolean loginStatus;

    public AuthorizationResponseModel getAuthorizationResponseModel() {
        return authorizationResponseModel;
    }

    public CustomError error;

    public CustomError getError() {
        return error;
    }

    public void setError(CustomError error) {
        this.error = error;
    }


    public void setAuthorizationResponseModel(AuthorizationResponseModel authorizationResponseModel) {
        this.authorizationResponseModel = authorizationResponseModel;
    }

    private AuthorizationResponseModel authorizationResponseModel;

    public boolean isLoginStatus() {
        return loginStatus;
    }

    public void setLoginStatus(boolean loginStatus) {
        this.loginStatus = loginStatus;
    }

    @Override
    public String toString(){
        return "{ loginStatus" + loginStatus +
                ", authorizationResponseModel" + authorizationResponseModel
                + " }";
    }




}
